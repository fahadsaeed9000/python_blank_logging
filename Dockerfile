FROM python:3.9 AS builder
RUN pip install pipenv
COPY Pipfile* /tmp/
RUN cd /tmp && pipenv lock --keep-outdated --requirements > requirements.txt
RUN pip install --user -r /tmp/requirements.txt

FROM python:3.9-slim
WORKDIR /code

COPY --from=builder /root/.local /root/.local
COPY ./ .
ENV PATH=/root/.local:$PATH

CMD [ "python", "-msrc.server" ]
