from flask import Flask
import datetime, logging, sys, json_logging

server = Flask(__name__)
json_logging.init_flask(enable_json=True)
json_logging.init_request_instrument(server)

# init the logger as usual
logger = logging.getLogger("test-logger")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))


@server.route("/health")
def hello():
    logger.info('hello')
    return {"status": "OK"}


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=80)
