
up:
	docker-compose up -d

shell:
	docker exec -it app-odoo-adapter /bin/bash

test:
	docker exec -it app-odoo-adapter coverage run -m pytest tests
	docker exec -it app-odoo-adapter coverage report

## consumers
product-consumer:
	docker exec -it app-odoo-adapter python3 -m  src.product_consumer

purchase-order-consumer:
	docker exec -it app-odoo-adapter python3 -m  src.purchase_order_consumer

sale-order-consumer:
	docker exec -it app-odoo-adapter python3 -m  src.order_request_consumer


