# app-odoo-adapter
Adapter to support consuming messages from rabbit mq and forwarding to odoo

## Getting started

- Make sure [odoo](https://git.cps.metro-markets.org/erp/odoo/odoo-dev-env) is up and  running with all submodules up to date
- `docker-compose up -d`

## Consume messages

Once you started one of the following consumers you can go to the Odoo RabbitMQ webinterface and see the corresponding queue. Feel free to publish messages to the queues and see if the import works.

### Product message
``make product-consumer``

publish message of type vendor.product.v1
### Sale Order Message
``make sale-order-consumer``

publish message of type delivery.order_request.v1

### Purchase Order Message
``make purchase-order-consumer``

publish message of type delivery.request.v1

## Run tests
``make test``
